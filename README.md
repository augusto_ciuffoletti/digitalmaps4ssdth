###Teaching material for  the tutorial on
#Digital Maps and Geographical Information Systems

###Digital Tools For Humanists Summer School 2019

This git repository contains the teaching material for the course on digital maps.

To download the repository as a zip archive click on the **Downloads** button --- last item on the left frame in this page --- and select **Download repository**.

The material is organized as follows

- slides: the slides of the course in a printable format
- Instructions: detailed instructions to carry out the excercises
- Docs: a few useful documents
- GPX: some GPX tracks to play with
- Maps: digital images to make experiments with georeferencing

I uploaded on Vimeo the following screencasts that illustrate some of the excercises:

- [qGis](https://vimeo.com/user5379251/review/340683752/1e15a8e3c8) Map creation with OSM raster, a new layer added with one point, point attributes management
- [Georeferencing] (https://vimeo.com/user5379251/review/341319281/e4d4cb71ec) georeferencing a hand-drawn map in png format over a OSM raster using qGis
- [OpenStreetMap](https://vimeo.com/user5379251/review/340684023/0a081f6602)  Create features on OSM (a point, a line, an area)
- [uMap](https://vimeo.com/user5379251/review/340683865/d4ae52ccc8) Features creation, adding links, images and movies in the description, sharing and downloading data

You can download the qGis tool (needed for the hands-on) from
[here](https://qgis.org/it/site/forusers/download.html). The recommended version is 3.4.6 "Madeira", available for Linux/Mac/Windows: other versions may have a slightly different interface. For Ubuntu the older version "Las Palmas", which is included in last LTS release, is also OK.

For the hands-on activity with your mobile phone we are going to use the "Gaia GPS" app, which is available for Android and iOS. There is an old manual in the Docs directory of this repository. You should register to the "Gaia GPS"  [web service ](https://www.gaiagps.com/login/) as a "free" user , also from your desktop.
